import socket
class webapp:

    def parse(self, received):
        recibido = received.decode()
        return recibido.split(' ')[1]

    def process(self, analyzed):
        http = "200 OK"
        html = "<html><body><h1>Hello World! Tu peticion es: " +analyzed+ "</h1>" \
        + "</body></html>"
        return http , html


    def __init__(self, ip, port):
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.bind((ip, port))
        mySocket.listen(5)
        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print("HTTP request received:")
            received = recvSocket.recv(2048)
            print(received)

            petition = self.parse(received)
            http, html = self.process(petition)

            response = "HTTP/1.1 " + http +"\r\n\r\n" \
            +html + "\r\n"

            recvSocket.send(response.encode('utf-8'))
            recvSocket.close()

if __name__=="__main__":
    webapp = webapp('localHost', 1234)