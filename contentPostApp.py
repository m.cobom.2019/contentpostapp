#Alumno: Mario Cobo Martínez
import webapp

formulario   = """
                <form name="formulario" action="" method="POST">
                <input type="text" name="contenido"> 
                <input type="submit" value="Enviar">
                </form>"""


class contentPostAPP(webapp.webapp):

    recursos = {'/':'p&aacute;gina principal',
               '/david':'p&aacute;gina de david'}

    def parse(self, received):
        recibido = received.decode()
        metodo = recibido.split(' ')[0]
        recurso = recibido.split(' ')[1]
        if metodo == "POST":
            body = recibido.split('\r\n\r\n')[1]
        else:
            body = None

        return metodo, recurso, body

    def process(self, analyzed):

        metodo, recurso, body = analyzed

        if metodo == "POST":
            self.recursos[recurso] = body
            http = "200 OK"
            html = "<html><body>Recurso solicitado: " + recurso + \
                   "<br>Contenido: " + self.recursos[recurso] + formulario + "</body></html>"
            #procedo a guardar el nuevo recorso en "recursos"
            contenido = body.split('contenido=')[1]
            print(contenido) #traza
            self.recursos['/'+contenido]= 'p&aacute;gina de:'+contenido
        else:
            if recurso in self.recursos.keys():
                http = "200 OK"
                html = "<html><body><h4>La p&aacute;gina ha sido encontrada</h4>" \
                       "Recurso solicitado: "+ recurso + \
                       "<br>" + "Contenido: " + self.recursos[recurso] + \
                       "<p>Introduzca un nuevo recurso </p>"+formulario+"</body></html>"

            else:
                http = "404 NOT FOUND"
                html = "<html><body><img src='https://upload.wikimedia.org/wikipedia/commons/9/9b/404-error-css.png'/></body></html>"
                return http, html

        return http, html


if __name__ == "__main__":
    contentPostAPP= contentPostAPP('localhost', 1239)